#!/usr/bin/env python
#
# Quick Zabbix external check to fetch mongodb server status
# Will be called from Zabbix server. This script must be placed in /etc/zabbix/externalscripts
# and made executable (chmod +x)
#
# See also  https://www.zabbix.com/documentation/2.0/manual/config/items/itemtypes/external
# Designed using python standard modules, as we don't know target system.
#
# Zabbix example item defintion
#
# type: external check 
# key: mongo_check2.py["-i","192.168.42.88","-p","10070","-q","/serverStatus","-j","extra_info/page_faults"]
# Type of information: Numeric (unsigned)
# Data Type: Decimal
# Update Interval: 60 seconds
# New Application: MongoDBStats
# 
# Note: the "-j" option is like a json path into the resulting json response, allowing us to select which
# parts of the json that should be examined.
#
# frank
#

# standard modules
import argparse
import json
import sys
import urllib2

# 3rd party modules
#import requests

# local modules


def do_main():
    """
    Parse command line args
    """
    #print sys.argv # for debug

    # Simplified argparse just for demo script

    parser = argparse.ArgumentParser()

    parser.add_argument('-i', action='store', dest='ip', required=True, help='ip_address')
    parser.add_argument('-p', action='store', dest='port', required=True, help='port')
    parser.add_argument('-q', action='store', dest='path', required=True, help='path')
    parser.add_argument('-j', action='store', dest='jsonpath', required=True, help='jsonpath')

    parser.add_argument('--version', action='version', version='%(prog)s 0.1.0')

    # parse args and process counter file(s) matching the input parameters
    results = parser.parse_args()

    # save args locally
    ip = results.ip
    port = results.port
    url_path = results.path
    jsonpath = results.jsonpath

    # create URL
    url = "http://" + ip + ":" + port + url_path
    # fetch data
    response = urllib2.urlopen(url)

    #print response
    response_json = json.load(response)
    
    # fetch json path args
    path_elements = jsonpath.split("/")

    #print path_elements

    # unroll loop for now
    # support 4 level json path query for now
    if len(jsonpath) == 0:
        # debug only, print all
        print response_json
    elif len(path_elements) == 1:
        print response_json[path_elements[0]]
    elif len(path_elements) == 2:
        print response_json[path_elements[0]][path_elements[1]]
    elif len(path_elements) == 3:
        print response_json[path_elements[0]][path_elements[1]][path_elements[2]]
    elif len(path_elements) == 4:
        print response_json[path_elements[0]][path_elements[1]][path_elements[2]][path_elements[3]]

    response.close()  # best practice to close the file



if __name__ == '__main__':
    do_main()

