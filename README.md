
A very simple Zabbix external check to fetch mongodb server status.

This is useful if you dont' have MMS installed, and just want to grab a few
interesting metrics without installing any Zabbix Agent on the box.

If you have agents installed, there are better ways to do this.

Will be called from Zabbix server. This script must be placed in /etc/zabbix/externalscripts
and made executable (chmod +x)

See also  https://www.zabbix.com/documentation/2.0/manual/config/items/itemtypes/external
 
Designed using python standard modules, as we don't know target system.

Zabbix example item defintion

    type: external check 
    key: mongo_check2.py["-i","192.168.42.88","-p","10070","-q","/serverStatus","-j","extra_info/page_faults"]
    Type of information: Numeric (unsigned)
    Data Type: Decimal
    Update Interval: 300 seconds
    New Application: MongoDBStats
 
Note: the "-j" option is like a json path into the resulting json response, allowing us to select which
parts of the json that should be examined.


For example, given this response from /serverStatus, we can select connections/current
using -j "connections/current", which would return 64.

    {
      "asserts": {
        "msg": 0, 
        "regular": 0, 
        "rollovers": 0, 
        "user": 0, 
        "warning": 0
      }, 
      "backgroundFlushing": {
        "average_ms": 9.13171335706547, 
        "flushes": 6461, 
        "last_finished": {
          "$date": 1408565593420
        }, 
        "last_ms": 0, 
        "total_ms": 59000
      }, 
      "connections": {
        "available": 19936, 
        "current": 64
      }, 
      <snip>
      
      
To test the script outside Zabbix, you can execute a command like

    /etc/zabbix/externalscripts/mongo_check2.py -i 192.168.44.88 -p 10077 -q "/serverStatus" -j "extra_info/page_faults"

    In this case the result is
    
    440260
    
          
Set the command line arguments to suit your environment.

